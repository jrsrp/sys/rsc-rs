# rss

## Summary

The Remote Sensing Sciences (RSS) crate is a [Rust](https://www.rust-lang.org/) library and command line application aiming to aid in remote sening processing for JRSRP. It is is many ways analogous (but with less features) to the rscutils python package.
It was mainly developed as a learning experience and to support other rust based projects, but some other RSS staff are also using the command line application. Some functions have been wrapped with py03 in order to be accessible via python. 

## Features

### CLI:

The CLI can be accesed via the rss container (`singularity run docker://artemis:5050/qld/des/rsc/rss:latest/ <COMMAND>`)   

```bash 
> singularity run docker://artemis:5050/qld/des/rsc/rss:latest/

Usage: rss <COMMAND>

Commands:
  get-imagery  Get imagery from several sources (apollo and dea are currently supported.)
  help         Print this message or the help of the given subcommand(s)

Options:
  -h, --help     Print help
  -V, --version  Print version
  
```

Currently the only available command is `get-imagery`. It allows getting images from different sources like DEA, Element84, Planetary computer and the HPC (apollo) with the same interface. This should for example help to make transition to  cloud processing seamless.

The tool has several arguments and options.

```bash
> singularity run docker://artemis:5050/qld/des/rsc/rss:latest/get-imagery 


Get imagery from several sources (apollo and dea are currently supported.)

Usage: rss get-imagery [OPTIONS] --collection <COLLECTION> --source <SOURCE> --start-date <START_DATE> --end-date <END_DATE> --max-cloud <MAX_CLOUD>

Options:
  -c, --collection <COLLECTION>
          [possible values: landsat5, landsat7, landsat8, landsat9, landsat-all, sentinel2]
  -s, --source <SOURCE>
          [possible values: dea, apollo, element]
  -t, --tile <TILE>
          Comma separated lists of scenes to get (i.e 55klr for Sentinell; p095r080 for Landsat)
  -b, --bbox <BBOX>
          Single bbox to intersect with; (order is ximin, xmax, ymin, ymax)
  -l, --layers <LAYERS>
          Layers of interest. Asset name for dea (nbart_red,nbart_nir); or stage code for apollo (aba, abb))
      --start-date <START_DATE>
          Start date in Year-month-day format (2022-01-01)
  -e, --end-date <END_DATE>
          End date in Year-month-day format (2022-01-10)
  -m, --max-cloud <MAX_CLOUD>
          Max percent of cloud cover
  -d, --dst <DST>
          Local directory in which to place the files [default: .]
      --local-stac <LOCAL_STAC>
          local stac filename [default: local.stac]
  -w, --crop-window <CROP_WINDOW>
          Only download part of the imagery. The window is defined as xmin, xmax,ymin,ymax
  -r, --crop-window-epsg <CROP_WINDOW_EPSG>
          
  -h, --help
          Print help
 
```
So for example if you want to get a Landsat 8 reflectance images from apollo (dbg layer) for the 2017-07-01 to 2017-07-30, intersecting a the 143.949,143.969,-14.468,-14.44 bounding box you could do: 

```
singularity run docker://artemis:5050/qld/des/rsc/rss:latest get-imagery\
               --collection landsat8\
               -s apollo\
               --start-date 2017-07-01\
               --end-date 2017-07-30\
               -m 100\
               -b 143.949,143.969,-14.468,-14.44\
               -l dbg\
               -d /tmp  
```
Similarly to ge the data from DEA:

```
singularity run docker://artemis:5050/qld/des/rsc/rss:latest get-imagery\
               --collection landsat8\
               -s dea\
               --start-date 2017-07-01\
               --end-date 2017-07-30\
               -m 100\
               -b 143.949,143.969,-14.468,-14.44\
               -l nbart_nir,nbart_swir_2,nbart_green,nbart_blue,nbart_red,nbart_swir_1
               -d /tmp  
```

In both cases it will donwload all the scenes matching the query and create a local stac catalog catalog with the metadata for the images. I tend to use the local stac file rather than the images, as input for my processing pipelines in the [eorst](https://gitlab.com/jrsrp/sys/eorst), see for [example](https://gitlab.com/jrsrp/sys/eorst/-/blob/main/examples/5_compute_ndvi_over_time_stac.rs#L81).

The tool also allows querry per tile or list of tiles by replacing the `-b` `--bbox` parameter by a `-t` `--tile` followed by the tile name.

```
singularity run docker://artemis:5050/qld/des/rsc/rss:latest get-imagery\
               --collection landsat8\
               -s dea\
               --start-date 2017-07-01\
               --end-date 2017-07-30\
               -m 100\
               -t p097r070\
               -l nbart_nir,nbart_swir_2,nbart_green,nbart_blue,nbart_red,nbart_swir_1\
               -d /tmp  
  
```

It also support partial reads (via vsicurl. i.e not supported in apollo, but will still crop the recalled images) of imagery using the `-w` `--crop-window` in Lon-Lat (epsg: 4326) arguments or in a different projection using `-r` `--crop-window-epsg`

```
singularity run docker://artemis:5050/qld/des/rsc/rss:latest get-imagery\
               --collection landsat8\
               -s dea\
               --start-date 2017-07-01\
               --end-date 2017-07-30\
               -m 100\
               -t p097r070\
               -l nbart_nir,nbart_swir_2,nbart_green,nbart_blue,nbart_red,nbart_swir_1\
              -w 143.95,143.96,-14.46,-14.44 \
               -d /tmp 
              

  
``` 

 ## Rust crate

Find the documentation [here](https://jrsrp.gitlab.io/sys/rss/rss/) 

## Python wrapper

Currently only the `get_s2_cloudless_dea` from the `masks.rs` submodule is available trough the python wrapper. The functions and cli are not available in the container but are installable via as a python package in the local python registry (`pip install rss`)
The function will download the sen2cloudsless cloud and shadow mask from DEA working out the corresponding query from the input filename, provided as first argument for the function. The second argument is the output filename.

```python
from rss import get_s2_cloudless_dea
get_s2_cloudless_dea("cemsre_t55kds_20230602_abam5.meta", "cemsre_t55kds_20230602_afzm5.img")
``` 

The python package also contains a cli script called `qv_s2cloudless` whcih also takes a qvf filename as input and a qvf filename as output.

# Setting up developement environments

## Rust

```bash
singularity shell docker://artemis:5050/qld/des/rsc/builder_rust:latest

# To build the crate:
cargo b  
```

## Python

The python code is avaliable in the rss subfolder. 

```bash
singularity shell docker://artemis:5050/qld/des/rsc/builder_rust:latest
python3 -m venv .venc
source .venv/bin/activate
pip install -U pip maturin patchelf

```