//! Module to handle input/output (i.e Download or recall imagery)
//! See [get method](QueryResult::get)

use crate::query::QueryResult;
use crate::utils::{Bbox, ImagerySource};
use crate::qvf::{QvfFilename, QvfFilenames};
use gdal::Dataset;
use stac::{ItemCollection, Asset, Item};
use std::path::PathBuf;
use anyhow::Result;
use std::str::FromStr;
use std::collections::HashMap;
use std::iter::FromIterator;
use log::{info, debug, warn};
use rayon::prelude::{IntoParallelIterator, ParallelIterator};
use std::process::Command;
use std::{thread, time};
use anyhow::bail;
use serde_json::json;
use std::sync::Arc;
use tokio::task;
use async_process::Command as Async_Command;
use std::env;
use tokio::sync::Semaphore;

/// Implement methods for query results.
impl QueryResult {
    pub fn get_remote(self) -> Result<ItemCollection> {
        let local_feature_collection = match self.source {
            ImagerySource::Apollo(_) => { bail!("Not supported")},
            ImagerySource::Dea(_) | ImagerySource::Element(_) => {
                let mut to_get= Vec::new();
                for item in &self.result.items {
                    let id = &item.id;
                    for (_, asset) in item.assets.clone() {
                        let href = asset.href;
                        to_get.push((id, href));
                    }
                }
                if !to_get.is_empty() {
                    info!("Getting metadata for {:?} items.", to_get.len());
                } else {
                    bail!("Nothing to get. Check your query!");
                }

                let root = match self.source {
                    ImagerySource::Dea(_) => "/vsicurl/https://data.dea.ga.gov.au",
                    ImagerySource::Element(_) => {
                        let r = match self.result.items[0].collection.clone() {Some(collection) => {match collection.as_str() {
                            "sentinel-2-l2a" => "/vsicurl/https://sentinel-cogs.s3.us-west-2.amazonaws.com",
                            "landsat-c2-l2" => "/vsis3/usgs-landsat",
                            _ => bail!("whot!"),
                        }},
                        None => bail!("No collection!")};
                        r
                    }
                    _ => todo!(),
                };
     //            let mut urls = Vec::new();
     //            to_get.into_iter().for_each(|(id, url)| {
     //                let loc = url.as_str().split("/").collect::<Vec<&str>>()[3..].join("/");
     //                let url = format!("{}/{}", root, loc);
     //                urls.push(url);
     // });
                let mut items = Vec::new();
                for item in self.result.items {
                    let mut local_assets: HashMap<String, Asset> = HashMap::new();
                    for (k, asset) in item.assets {
                        let mut local_asset = asset.to_owned();
                        // let href_parts = asset.href.split("/").collect::<Vec<&str>>();
                        // let file_name = href_parts.last().unwrap();
                        // let local_href = PathBuf::from(".").join(id).join(file_name);
                        let loc = asset.href.as_str().split('/').collect::<Vec<&str>>()[3..].join("/");
                        let url = format!("{}/{}", root, loc);
                        local_asset.href = url;
                        local_assets.insert(k, local_asset);
                    }
                    let mut f_item = Item::new(item.id);
                    f_item.extensions = item.extensions;
                    f_item.geometry = item.geometry;
                    f_item.bbox = item.bbox;
                    f_item.properties = item.properties;
                    f_item.links = item.links;
                    f_item.assets = local_assets;
                    f_item.collection = item.collection;
                    f_item.additional_fields = item.additional_fields;
                    items.push(f_item);
                }

                
                ItemCollection::from_iter(items)
                
            }
        };
Ok(local_feature_collection)

    }
    /// Get images from a query result.
    /// Example
    /// ```
    /// use stac::ItemCollection;
    /// use rss::DEA;
    /// use rss::query::ImageQueryBuilder;
    /// use rss::qvf::Collection;
    /// use rss::utils::Intersects;
    /// use rss::utils::Cmp;
    /// use std::path::PathBuf;
    /// use std::ops::Deref;
    /// use chrono::NaiveDate;
    ///
    /// let source = DEA.deref().clone();
    /// // This query will yield 52 Items (metadata only)
    /// let query = ImageQueryBuilder::new(
    ///    source,
    ///    Collection::Sentinel2,
    ///    Intersects::Scene(vec!["54kvu"]),
    ///    &["nbart_red", "nbart_nir_1"])
    ///    .start_date(NaiveDate::parse_from_str("20210101", "%Y%m%d").unwrap())
    ///    .end_date(NaiveDate::parse_from_str("20210111", "%Y%m%d").unwrap())
    ///    .cloudcover((Cmp::Less, 5))
    ///    .build();
    /// let tmp_dir = PathBuf::from("/scratch/rsc8/hardtkel/tmp2");
    /// //Running the next line will download 52 images. 
    /// //let feature_collection: ItemCollection = query
    /// //  .get(&tmp_dir, None, None)
    /// //   .expect("Could not download files");
    
    /// ```
    pub fn get(
        self,
        to: &PathBuf,
        crop_window: Option<Bbox>,
        crop_window_epsg: Option<&str>,
    ) -> Result<ItemCollection> {
        std::fs::create_dir(to).unwrap_or(());
        let local_feature_collection = match self.source {
            ImagerySource::Apollo(_) => {
                if let Some(_w) = crop_window {
                    unimplemented!("This hasn't been implemented yet.");
                }
                let mut to_recall = Vec::new();
                for item in &self.result.items {
                    for (_, asset) in item.assets.clone() {
                        let href = asset.href;
                        to_recall.push(QvfFilename::from_str(&href).unwrap());
                    }
                }

                if !to_recall.is_empty() {
                    let qvf_filenames = QvfFilenames {
                        qvf_filenames: to_recall,
                    };
                    let _ = qvf_filenames
                        .recall(to)
                        .expect("Could not recall the files");
                } else {
                    warn!("Noting to recall")
                }

                let mut items = Vec::new();
                for item in self.result.items {
                    let mut local_assets: HashMap<String, Asset> = HashMap::new();
                    info!("Working on item: {:?}", item);
                    for (_, multiband_asset) in &item.assets {
                        let href_parts = multiband_asset.href.split('/').collect::<Vec<&str>>();
                        let file_name = href_parts.last().unwrap();
                        let multiband_filename = to.join(file_name);
                        let ds = Dataset::open(multiband_filename.clone())?;
                        let n_bands = ds.raster_count();
                        info!("Asset has {} bands", n_bands);
                        let stem = multiband_filename.file_stem().unwrap().to_str().unwrap();
                        let _extension = multiband_filename.extension().unwrap().to_str().unwrap();
                        for band_id in 0..n_bands {
                            let single_band_stem = format!("{}_b{}", stem, band_id + 1);
                            let single_band = format!("{}.{}", single_band_stem, "tif");
                            let single_band = multiband_filename.parent().unwrap().join(single_band);
                            
                         let argv = &[
                              "gdal_translate",
                              "-b",
                              &format!("{}", band_id + 1),
                              "-q",
                              multiband_filename.to_str().unwrap(),
                              (single_band.to_str().unwrap()),
                        ];
                std::process::Command::new(argv[0])
                    .args(&argv[1..])
                    .spawn()
                    .expect("failed to start creating vrt")
                    .wait()
                    .expect("failed to wait for the vrt");
                            
                            
                           let mut single_band_asset = multiband_asset.to_owned();
                           single_band_asset.href = single_band.to_str().unwrap().to_string();
   //update additional fields/
                            let bn = band_id +1;
                        let mut additional_fields = single_band_asset.additional_fields;
                            let asset_name =  format!("{}_{bn}", single_band_asset.title.clone().unwrap());
                        additional_fields.insert(
                           "eo:bands".to_string(),
                           json!([{"common_name": asset_name}]),);
                        single_band_asset.additional_fields = additional_fields;
                        // insert to local assets HashMap!
                        println!("Added: {single_band_asset:?}");
                        local_assets.insert(asset_name, single_band_asset);
                            
                    }
                        
                        
                    }
                    // for (k, asset) in item.assets {
                    //     let mut local_asset = asset.to_owned();
                    //     let href_parts = asset.href.split('/').collect::<Vec<&str>>();
                    //     let file_name = href_parts.last().unwrap();
                    //     let local_href = to.join(file_name);
                    //     local_asset.href = local_href.to_str().unwrap().to_string();

                    //     //update additional fields/
                    //     let mut additional_fields = asset.additional_fields;
                    //     additional_fields.insert(
                    //        "eo:bands".to_string(),
                    //        json!([{"common_name": asset.title.unwrap()}]),);
                    //     local_asset.additional_fields = additional_fields;
                    //     // insert to local assets HashMap!
                    //     println!("Added: {local_asset:?}");
                    //     local_assets.insert(k, local_asset);
                    // }
                    let mut f_item = Item::new(item.id);
                    f_item.extensions = item.extensions;
                    f_item.geometry = item.geometry;
                    f_item.bbox = item.bbox;
                    f_item.properties = item.properties;
                    f_item.links = item.links;
                    f_item.assets = local_assets;
                    f_item.collection = item.collection;
                    f_item.additional_fields = item.additional_fields;
                    items.push(f_item);
                }
                
                
                
                    
                ItemCollection::from(items)
                
                // ItemCollection::from_iter(items)
            }
            ImagerySource::Dea(_) | ImagerySource::Element(_) => {
                let mut to_download = Vec::new();
debug!("Items to download found {:?}", self.result.items);
                
                debug!("Items to download found {:?}", self.result.items);
                for item in &self.result.items {
                    let id = &item.id;
                    for (_, asset) in item.assets.clone() {
                        let href = asset.href;
                        to_download.push((id, href));
                    }
                }
                if !to_download.is_empty() {
                    info!("Downloading {:?} files.", to_download.len());
                    debug!("{:?}", to_download);
                } else {
                    info!("Nothing to download. Check your query!");
                }

                
                env::var("RSS_DOWNLOAD_THREADS").unwrap_or("8".to_string());
                
                // rayon::ThreadPoolBuilder::new().num_threads(n_threads)
                //  .build_global()
                //  .unwrap();
                

                let root = match self.source {
                    ImagerySource::Dea(_) => "/vsicurl/https://data.dea.ga.gov.au",
                    ImagerySource::Apollo(_) => todo!(),
                    ImagerySource::Element(_) => {
                        let r = match self.result.items[0].collection.clone() {Some(collection) => {match collection.as_str() {
                            "sentinel-2-l2a" => "/vsicurl/https://sentinel-cogs.s3.us-west-2.amazonaws.com",
                            "landsat-c2-l2" => "/vsis3/usgs-landsat",
                            _ => bail!("whot!"),
                        }},
                        None => bail!("No collection!")};
                        r
                        // "https://sentinel-cogs.s3.us-west-2.amazonaws.com"
                    }
                };

                to_download.into_par_iter().for_each(|(id, url)| {
                    
                    //let url = Url::parse(&url).unwrap();
                    let loc = url.as_str().split('/').collect::<Vec<&str>>()[3..].join("/");
                    //let root = root;
                    let url = &format!("{}/{}", root, loc);
                    //let url = Url::parse(url_s).expect("Could not parse URL");
                    let href_parts = url.as_str().split('/').collect::<Vec<&str>>();
                    let file_name = href_parts.last().unwrap();
                    let parent = to.join(id);

                    debug!(
                        "Attempting to create {parent:?}"
                    );

                    //let vsicurl = format!("/vsicurl/{url}");
                    debug!("url {url:?}");

                    std::fs::create_dir_all(&parent).expect("Could not create the parent folder");
                    let local_href = parent.join(file_name);
                    let mut cmd = Command::new("gdal_translate");
                    cmd.arg("-q");
                    if let Some(w) = crop_window {
                        
                        cmd.args([
                            ("-projwin"),
                            &format!("{}", w.xmin),
                            &format!("{}", w.ymax),
                            &format!("{}", w.xmax),
                            &format!("{}", w.ymin),
                        ]);
                    }
                    if let Some(w_epsg) = crop_window_epsg {
                        cmd.args(["-projwin_srs", &format!("EPSG:{w_epsg}")]);
                    }
 
                    cmd.arg( url.clone() )           // remote file
                       .arg( local_href.clone() );           // local file
                    let mut was_downloaded = local_href.exists() ; // check if file is alredy there... 
                    let mut n_retry = 1;
                    let max_retry = 20;
                    while (n_retry <= max_retry) & ! was_downloaded {
                        if n_retry > 5 {
                            let sleep_time = time::Duration::from_secs(10*n_retry);
                            thread::sleep(sleep_time);
                        }
                        cmd.spawn()
                            .expect("failed creating local file")
                            .wait()
                            .expect("failed to get the file");
                        was_downloaded = local_href.exists();
                        debug!("{n_retry:?} attempt to download {url:?} to {local_href:?} -> exist: {was_downloaded:?}"); 
                        n_retry += 1;
                    }
                    if ! was_downloaded {
                        panic!("The file {:?} could not be downloaded", url);
                    }
                });

                let mut items = Vec::new();
                for item in self.result.items {
                    let id = &item.id;
                    let mut local_assets: HashMap<String, Asset> = HashMap::new();
                    for (k, asset) in item.assets {
                        let mut local_asset = asset.to_owned();
                        let href_parts = asset.href.split('/').collect::<Vec<&str>>();
                        let file_name = href_parts.last().unwrap();
                        let local_href = PathBuf::from(to).join(id).join(file_name);
                        local_asset.href = local_href.to_str().unwrap().to_string();
                        local_assets.insert(k, local_asset);
                    }
                    let mut f_item = Item::new(item.id);
                    f_item.extensions = item.extensions;
                    f_item.geometry = item.geometry;
                    f_item.bbox = item.bbox;
                    f_item.properties = item.properties;
                    f_item.links = item.links;
                    f_item.assets = local_assets;
                    f_item.collection = item.collection;
                    f_item.additional_fields = item.additional_fields;
                    items.push(f_item);
                }

                
                ItemCollection::from_iter(items)
            }
        };
        Ok(local_feature_collection)
    }

    pub async fn get_async(
        self,
        to: &PathBuf,
        n_downloads: usize,
        crop_window: Option<Bbox>,
        crop_window_epsg: Option<&str>,
    ) -> Result<ItemCollection> {
        std::fs::create_dir(to).unwrap_or(());
        let semaphore = Arc::new(Semaphore::new(n_downloads));
        let local_feature_collection = match self.source {
            ImagerySource::Apollo(_) => {
                panic!("Async methods not supported in Apollo ");
            }
            ImagerySource::Dea(_) | ImagerySource::Element(_) => {
                let mut to_download = Vec::new();
                let result = self.result.clone();
                for item in result.items {
                    for (_, asset) in item.assets.clone() {
                        let href = asset.href;
                        to_download.push((item.id.clone(), href));
                    }
                }
                if !to_download.is_empty() {
                    info!("Downloading {:?} files.", to_download.len());
                    debug!("{:?}", to_download);
                    
                } else {
                    info!("Nothing to download. Check your query!");
                }


                let root = match self.source {
                    ImagerySource::Dea(_) => "/vsicurl/https://data.dea.ga.gov.au",
                    ImagerySource::Apollo(_) => todo!(),
                    ImagerySource::Element(_) => {
                        let r = match self.result.items[0].collection.clone() {Some(collection) => {match collection.as_str() {
                            "sentinel-2-l2a" => "/vsicurl/https://sentinel-cogs.s3.us-west-2.amazonaws.com",
                            "landsat-c2-l2" => "/vsis3/usgs-landsat",
                            _ => bail!("whot!"),
                        }},
                        None => bail!("No collection!")};
                        r
                    }
                };

                    
            let path = Arc::new(PathBuf::from(to).to_owned());
            let crop_window_epsg_arc = Arc::new(crop_window_epsg.map(|s| s.to_owned()));         
                
                
                let overwrite = match env::var("RSS_OVERWRITE") {
                    Ok(value) => match value.as_str() {
                        "true" => true,
                        "false" => false,
                             _ => false
                    },
                    Err(_) => false, 
                };
                
            let tasks = to_download.into_iter().map(|(id, url)| {
            let path_clone = Arc::clone(&path); // Create a new reference to the Arc inside the closure
            let crop_window_epsg = Arc::clone(&crop_window_epsg_arc);
            let semaphore = Arc::clone(&semaphore);
            task::spawn(async move {
            let permit = semaphore.clone().acquire_owned().await.unwrap();
                
                let path_ref = PathBuf::from(path_clone.as_ref());  
                let loc = url.as_str().split('/').collect::<Vec<&str>>()[3..].join("/");
                    let url = &format!("{}/{}", root, loc);
                    let href_parts = url.as_str().split('/').collect::<Vec<&str>>();
                    let file_name = href_parts.last().unwrap();
                    let parent = path_ref.join(id);
                    debug!(
                        "Attempting to create {parent:?}"
                    );

                    debug!("url {url:?}");

                    std::fs::create_dir_all(&parent).expect("Could not create the parent folder");
                    let local_href = parent.join(file_name);
                    
                    let was_downloaded = local_href.exists() ; // check if file is alredy there...
                    if !was_downloaded | overwrite {
                        let mut cmd = Async_Command::new("gdal_translate");
                        cmd.arg("-q");
                        if let Some(w) = crop_window {      
                            cmd.args([
                                ("-projwin"),
                                &format!("{}", w.xmin),
                                &format!("{}", w.ymax),
                                &format!("{}", w.xmax),
                                &format!("{}", w.ymin),
                            ]);
                        }
                        if let Some(w_epsg) = crop_window_epsg.as_ref() {
                            cmd.args(["-projwin_srs", &format!("EPSG:{w_epsg}")]);
                        }
                        cmd.arg( url.clone() )           // remote file
                            .arg( local_href.clone() );
                            
                        cmd.output().await.unwrap();
                         drop(permit);
                        };
                    
    })        
    });
    let _ = futures::future::join_all(tasks).await;
                
                // for (id, url) in to_download {

                // to_download.into_par_iter().for_each(|(id, url)| {
 
                //     //let url = Url::parse(&url).unwrap();
                //     //let root = root;
                //     //let url = Url::parse(url_s).expect("Could not parse URL");
                //     let mut was_downloaded = local_href.exists() ; // check if file is alredy there... 
                //     let mut n_retry = 1;
                //     let max_retry = 20;
                //     while (n_retry <= max_retry) & ! was_downloaded {
                //         if n_retry > 5 {
                //             let sleep_time = time::Duration::from_secs(10*n_retry);
                //             thread::sleep(sleep_time);
                //         }
                //         cmd.spawn()
                //             .expect("failed creating local file")
                //             .wait()
                //             .expect("failed to get the file");
                //         was_downloaded = local_href.exists();
                //         debug!("{n_retry:?} attempt to download {url:?} to {local_href:?} -> exist: {was_downloaded:?}"); 
                //         n_retry += 1;
                //     }
                //     if ! was_downloaded {
                //         panic!("The file {:?} could not be downloaded", url);
                //     }
                // });

                let mut items = Vec::new();
                for item in self.result.items {
                    let id = &item.id;
                    let mut local_assets: HashMap<String, Asset> = HashMap::new();
                    for (k, asset) in item.assets {
                        let mut local_asset = asset.to_owned();
                        let href_parts = asset.href.split('/').collect::<Vec<&str>>();
                        let file_name = href_parts.last().unwrap();
                        let local_href = PathBuf::from(to).join(id).join(file_name);
                        local_asset.href = local_href.to_str().unwrap().to_string();
                        local_assets.insert(k, local_asset);
                    }
                    let mut f_item = Item::new(item.id);
                    f_item.extensions = item.extensions;
                    f_item.geometry = item.geometry;
                    f_item.bbox = item.bbox;
                    f_item.properties = item.properties;
                    f_item.links = item.links;
                    f_item.assets = local_assets;
                    f_item.collection = item.collection;
                    f_item.additional_fields = item.additional_fields;
                    items.push(f_item);
                }

                
                ItemCollection::from_iter(items)
            }
        };
        Ok(local_feature_collection)
    }

}


