//! The Remote Sensing Sciences (RSS) crate is a [Rust](https://www.rust-lang.org/) library and command line application aiming to aid in remote sening processing for JRSRP. It is is many ways analogous to (but with less features than) the rscutils python package. It also has some functions to help working with stac catalogues.

//! It was mainly developed as a learning experience and to support other rust based projects, but some other RSS staff are also using the command line application. Some functions have been wrapped with py03 in order to be accessible via python.
//!
//!
pub mod io;
pub mod masks;
pub mod query;
pub mod qvf;
pub mod stac;
pub mod utils;

use crate::qvf::Collection;
use lazy_static::lazy_static;
use std::collections::HashMap;
use utils::{ImageryProvider, ImageryProviderType, ImagerySource};

// Define some constants that will be used in other parts of the project.
// There must be a better way to do this!?
lazy_static! {
    /// Digital Earth Australia image source.
    pub static ref DEA: ImagerySource = ImagerySource::Dea(ImageryProvider {
        provider_type: ImageryProviderType::Stac,
        url: "https://explorer.sandbox.dea.ga.gov.au/stac/search".to_owned()
    });
    /// Apollo image source.
    pub static ref APOLLO: ImagerySource = ImagerySource::Apollo(ImageryProvider {
        provider_type: ImageryProviderType::Local,
        url: "/apollo".to_owned()
    });
    /// Element84 image source.
    pub static ref ELEMENT84: ImagerySource = ImagerySource::Element(ImageryProvider {
        provider_type: ImageryProviderType::Stac,
        url: "https://earth-search.aws.element84.com/v1/search".to_owned()
    });
}

lazy_static! {
    static ref COLLECTION_MAPPINGS: HashMap<ImagerySource, HashMap< Collection , Option<&'static str>  >> = {
        let mut cm = HashMap::new();
        // for dea:
        let source = DEA.deref().clone();
        let mut cc = HashMap::new();
        cc.insert(Collection::Landsat5, Some("ga_ls5t_ard_3"));
        cc.insert(Collection::Landsat7, Some("ga_ls7e_ard_3"));
        cc.insert(Collection::Landsat8, Some("ga_ls8c_ard_3"));
        cc.insert(Collection::Landsat9, Some("ga_ls9c_ard_3"));
        cc.insert(Collection::Sentinel2, Some("ga_s2am_ard_3,ga_s2bm_ard_3"));
        cc.insert(Collection::LandsatAll, Some("ga_ls5t_ard_3,ga_ls7e_ard_3,ga_ls8c_ard_3,ga_ls9c_ard_3")); // this is a workaround. Should be able to pass multiple collections via the cli
        cm.insert(source, cc);
        // for dea:
        let source = APOLLO.deref().clone();
        let mut cc = HashMap::new();
        cc.insert(Collection::Landsat5, None);
        cc.insert(Collection::Landsat7, None);
        cc.insert(Collection::Landsat8, None);
        cc.insert(Collection::Landsat9, None);
        cc.insert(Collection::Sentinel2, None);
        cm.insert(source, cc);
        // for dea:
        let source = ELEMENT84.deref().clone();
        let mut cc = HashMap::new();
        cc.insert(Collection::Landsat5, None);
        cc.insert(Collection::Landsat7, None);
        cc.insert(Collection::Landsat8, Some("landsat-c2-l2"));
        cc.insert(Collection::Landsat9, None);
        cc.insert(Collection::Sentinel2, Some("sentinel-2-l2a"));
        cm.insert(source, cc);

        cm
    };

}

#[cfg(test)]
mod tests {
    use std::{path::PathBuf, str::FromStr};

    use chrono::NaiveDate;

    use crate::qvf::{
        Collection, Extension, ImageType, Instrument, Product, QvfDate, QvfFields, QvfFilename,
        QvfFilenames, Satellite,
    };
    

    // #[test]
    // fn test_query_dea_sentinel() {
    //     let query = ImageQueryBuilder::new(
    //         ImagerySource::Dea,
    //         Collection::Sentinel2,
    //         Intersects::Scene((&["56jmr"]).to_vec()),
    //         &["nbart_red"],
    //     )
    //     .start_date(NaiveDate::parse_from_str("20220101", "%Y%m%d").unwrap())
    //     .end_date(NaiveDate::parse_from_str("20220501", "%Y%m%d").unwrap())
    //     .landcover((Cmp::Greater, 10))
    //     .cloudcover((Cmp::Less, 50))
    //     .build();
    //     println!("{query:?}");
    // }

    // #[test]
    // fn test_from_query() {
    //     let query = ImageQueryBuilder::new(
    //         ImagerySource::Apollo,
    //         Collection::Sentinel2,
    //         Intersects::Scene((["t56jmr"]).to_vec()),
    //         &["aba"],
    //     )
    //     .start_date(NaiveDate::parse_from_str("20220101", "%Y%m%d").unwrap())
    //     .end_date(NaiveDate::parse_from_str("20220501", "%Y%m%d").unwrap())
    //     .landcover((Cmp::Greater, 10))
    //     .cloudcover((Cmp::Less, 50))
    //     .build();
    // }
    // #[test]
    // fn test_query_landsat_dea() {
    //     let query = ImageQueryBuilder::new(
    //         ImagerySource::Dea,
    //         Collection::Landsat8,
    //         Intersects::Scene(["p091r077"].to_vec()),
    //         &["nbart_red"],
    //     )
    //     .start_date(NaiveDate::parse_from_str("20220101", "%Y%m%d").unwrap())
    //     .end_date(NaiveDate::parse_from_str("20220501", "%Y%m%d").unwrap())
    //     .landcover((Cmp::Greater, 10))
    //     .cloudcover((Cmp::Less, 50))
    //     .build();
    //     query.get(&PathBuf::from("/tmp")).unwrap();
    // }

    #[test]
    fn test_qvf_from_str() {
        let name = "/scratch/rsc8/hardtkel/tmp/cfmsre_t56jmr_20220104_abam6.img";
        let qvf = QvfFilename::from_str(name).unwrap();
        println!("{qvf}");
    }

    // #[test]
    // fn test_recall_multiple() {
    //     let query = ImageQueryBuilder::new(
    //         ImagerySource::Apollo,
    //         Collection::Sentinel2,
    //         Intersects::Scene(&["t56jmr"]),
    //         &["aba"],
    //     )
    //     .start_date(NaiveDate::parse_from_str("20220101", "%Y%m%d").unwrap())
    //     .end_date(NaiveDate::parse_from_str("20220501", "%Y%m%d").unwrap())
    //     .landcover((Cmp::Greater, 10))
    //     .cloudcover((Cmp::Less, 50))
    //     .build().expect("Invalid query");
    //     qvfs.recall(&PathBuf::from("/tmp"))
    //         .expect("Unable to recall files");
    // }

    // #[test]
    // fn test_dirname() {
    //     let qvfn = QvfFilename::from_str("cfmsre_t55kgs_20180531_abam5.img")
    //         .expect("Could not parse the file name.");
    //     let expected = PathBuf::from("/apollo/imagery/rsc/sentinel2/t55k/t55kgs/2018/201805/");
    //     let dir = qvfn.qv_dir().unwrap();
    //     assert_eq!(expected, dir);

    //     let qvfn = QvfFilename::from_str("l8olre_p092r078_20220104_da1m5.img")
    //         .expect("Could not parse the file name.");
    //     let expected =
    //         PathBuf::from("/apollo/imagery/rsc/landsat/landsat57tm/wrs2/092_078/2022/202201/");
    //     let dir = qvfn.qv_dir().unwrap();
    //     assert_eq!(expected, dir);
    // }

    // #[test]
    // fn test_recall() {
    //     let qvfn = QvfFilename::from_str("cfmsre_t55kgs_20180531_abam5.img")
    //         .expect("Could not parse the file name.");
    //     let qvfn = QvfFilename::from_str("l8olre_p092r078_20220104_da1m5.img")
    //         .expect("Could not parse the file name.");
    //     qvfn.recall(PathBuf::from("/tmp"));
    //     // let qvfn = QvfFilename::from_str("lztmre_p094r075_m201903201905_djam5.img")
    //     //     .expect("Could not parse the file name.");
    // }

    // #[test]
    // fn test_date_format() {
    //     let file = "cfmsre_t55kgs_20180531_abam5.img";
    //     let qvfn = QvfFilename::from_str(file).expect("Could not parse the file name.");
    //     println!("{}", qvfn.date.format("%Y"));
    // }

    #[test]
    pub(crate) fn test_parse() {
        let file = "cfmsre_t55kgs_20180531_abam5.img";
        let qvfn = QvfFilename::from_str(file).expect("Could not parse the file name.");
        let expected = QvfFilename {
            satellite: Satellite::cf,
            instrument: Instrument::ms,
            product: Product::re,
            scene: "t55kgs".to_string(),
            date: QvfDate::Date(NaiveDate::parse_from_str("20180531", "%Y%m%d").unwrap()),
            stage_code: "aba".to_string(),
            zone: "m5".to_string(),
            extension: Extension::img,
            image_type: ImageType::Scene,
            collection: Collection::Sentinel2,
            location: Some(PathBuf::from("")),
            extra_fields: None,
        };
        assert_eq!(qvfn, expected);

        let file = "lztmre_p093r088_m202203202205_dimm4.img";
        let _qvfn = QvfFilename::from_str(file).expect("Could not parse the file name.");
    }
    #[test]
    fn test_change_sat() {
        let file = "cfmsre_t55kgs_20180531_abam5.img";
        let mut qvfn = QvfFilename::from_str(file).expect("Could not parse the file name.");
        qvfn = qvfn.change_satellite(Satellite::l7);
        let expected = QvfFilename {
            satellite: Satellite::l7,
            instrument: Instrument::ms,
            product: Product::re,
            scene: "t55kgs".to_string(),
            date: QvfDate::Date(NaiveDate::parse_from_str("20180531", "%Y%m%d").unwrap()),
            stage_code: "aba".to_string(),
            zone: "m5".to_string(),
            extension: Extension::img,
            image_type: ImageType::Scene,
            collection: Collection::Sentinel2,
            location: Some(PathBuf::from("")),
            extra_fields: None,
        };
        assert_eq!(qvfn, expected);
    }
    #[test]
    fn test_change_inst() {
        let file = "cfmsre_t55kgs_20180531_abam5.img";
        let mut qvfn = QvfFilename::from_str(file).expect("Could not parse the file name.");
        qvfn = qvfn.change_instrument(Instrument::tm);
        let expected = QvfFilename {
            satellite: Satellite::cf,
            instrument: Instrument::tm,
            product: Product::re,
            scene: "t55kgs".to_string(),
            date: QvfDate::Date(NaiveDate::parse_from_str("20180531", "%Y%m%d").unwrap()),
            stage_code: "aba".to_string(),
            zone: "m5".to_string(),
            extension: Extension::img,
            image_type: ImageType::Scene,
            collection: Collection::Sentinel2,
            location: Some(PathBuf::from("")),
            extra_fields: None,
        };
        assert_eq!(qvfn, expected);
    }

    #[test]
    fn test_sort_by_dates() {
        let file_1 = QvfFilename::from_str("cfmsre_t55kgs_20180531_abam5.img").unwrap();
        let file_2 = QvfFilename::from_str("cfmsre_t55kgs_20180528_abam5.img").unwrap();
        let file_3 = QvfFilename::from_str("cfmsre_t55kgs_20170430_abam5.img").unwrap();
        let mut qvfs = QvfFilenames {
            qvf_filenames: vec![file_1, file_2, file_3],
        };
        qvfs.sort_by(QvfFields::Date);
        let file_1 = QvfFilename::from_str("cfmsre_t55kgs_20180531_abam5.img").unwrap();
        let file_2 = QvfFilename::from_str("cfmsre_t55kgs_20180528_abam5.img").unwrap();
        let file_3 = QvfFilename::from_str("cfmsre_t55kgs_20170430_abam5.img").unwrap();
        let expected = QvfFilenames {
            qvf_filenames: vec![file_3, file_2, file_1],
        };
        assert_eq!(qvfs, expected);
    }

    #[test]
    fn test_change_prod() {
        let file = "cfmsre_t55kgs_20180531_abam5.img";
        let mut qvfn = QvfFilename::from_str(file).expect("Could not parse the file name.");
        qvfn = qvfn.change_product(Product::pa);
        let expected = QvfFilename {
            satellite: Satellite::cf,
            instrument: Instrument::ms,
            product: Product::pa,
            scene: "t55kgs".to_string(),
            date: QvfDate::Date(NaiveDate::parse_from_str("20180531", "%Y%m%d").unwrap()),
            stage_code: "aba".to_string(),
            zone: "m5".to_string(),
            extension: Extension::img,
            image_type: ImageType::Scene,
            collection: Collection::Sentinel2,
            location: Some(PathBuf::from("")),
            extra_fields: None
        };
        assert_eq!(qvfn, expected);
    }
}
