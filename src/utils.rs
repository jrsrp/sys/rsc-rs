//! Module defining some base level structures.
use anyhow::Result;
use clap;
use clap::Parser;
use serde::{Deserialize, Serialize};
use std::num::ParseIntError;
use std::str::FromStr;
use strum_macros::EnumIter;
#[derive(Debug, Clone, clap::ValueEnum, Copy)]
pub enum Cmp {
    Less,
    Greater,
    LessEqual,
    GreaterEqual,
    Equal,
}

#[derive(Debug, Clone, clap::ValueEnum, PartialEq, Copy, Eq, Hash, Default)]
pub enum ImageryProviderType {
    Local,
    #[default]
    Stac,
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, EnumIter)]
pub enum ImagerySource {
    Dea(ImageryProvider),
    Apollo(ImageryProvider),
    Element(ImageryProvider),
}

#[derive(Debug, Clone, PartialEq, Eq, Hash, Default)]
pub struct ImageryProvider {
    pub provider_type: ImageryProviderType,
    pub url: String,
}

#[derive(Debug, Clone, clap::ValueEnum)]
pub enum ImagerySourceClap {
    Dea,
    Apollo,
    Element,
}

impl ImagerySource {
    /// Get Url for the Imagerey Source.
    ///
    /// Examples:
    /// ```
    /// use rss::DEA;
    ///
    /// let source = &DEA;
    /// let url = source.get_url();
    /// assert_eq!(url, "https://explorer.sandbox.dea.ga.gov.au/stac/search")
    /// ```  

    pub fn get_url(&self) -> String {
        let url: String = match &self {
            ImagerySource::Dea(p) => String::from(&p.url),
            ImagerySource::Apollo(p) => String::from(&p.url),
            ImagerySource::Element(p) => String::from(&p.url),
        };
        url
    }

    /// Get name for the Imagerey Source.
    ///
    /// Examples:
    /// ```
    /// use rss::DEA;
    ///
    /// let source = &DEA;
    /// let name = &source.name();
    /// assert_eq!(name, "DEA")
    /// ```  

    pub fn name(&self) -> String {
        let name: String = match &self {
            ImagerySource::Dea(_) => "DEA".to_owned(),
            ImagerySource::Apollo(_) => "Apollo".to_owned(),
            ImagerySource::Element(_) => "Element84".to_owned(),
        };
        name
    }
}

#[derive(Clone, Copy, Debug, Serialize, Deserialize, Parser)]

pub struct Bbox {
    pub xmin: f32,
    pub ymin: f32,
    pub xmax: f32,
    pub ymax: f32,
}

impl FromStr for Bbox {
    type Err = ParseIntError;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let parts: Vec<&str> = s.split(',').collect();
        Ok(Bbox {
            xmin: parts[0].parse().unwrap(),
            xmax: parts[1].parse().unwrap(),
            ymin: parts[2].parse().unwrap(),
            ymax: parts[3].parse().unwrap(),
        })
    }
}

use gdal::vector::Geometry;
#[derive(Clone)]
pub enum Intersects<'a> {
    Scene(Vec<&'a str>),
    Bbox(Bbox),
    Polygon(Geometry),
}

pub fn bbox_from_polygon(polygon: &gdal::vector::Geometry) -> Bbox {
    let envelope = polygon.envelope();

    
    Bbox {
        xmin: envelope.MinX as f32,
        ymin: envelope.MinY as f32,
        xmax: envelope.MaxX as f32,
        ymax: envelope.MaxY as f32,
    }
}
