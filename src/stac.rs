//! Module with methods to work with STAC structs (Itmes, Assets, etc.)
use std::collections::HashMap;

use crate::utils::Cmp;
use anyhow::Result;
use stac::{Asset, ItemCollection};
use std::iter::FromIterator;

pub fn filter_assets_by_key(
    assets: HashMap<String, Asset>,
    keys: &[&str],
) -> HashMap<String, Asset> {
    let mut res: HashMap<String, Asset> = HashMap::new();
    let available_keys = assets.keys().clone();
    for key in keys.iter() {
        let (k, v) = assets.get_key_value(*key).unwrap_or_else(|| {
            panic!(
                "{}",
                format!("Asset doesn't contain key {key}. Available keys: {available_keys:?}")
            )
        });
        res.insert(k.to_string(), v.to_owned());
    }
    res
}

pub fn filter_tile(
    items: ItemCollection,
    field_name: &str,
    values: &Vec<String>,
) -> Result<ItemCollection> {
    let mut filtered_items = Vec::new();
    

    for item in items.items {
        // info!("item {item:?}");
        let item_value = item.properties.additional_fields[field_name]
            .as_str()
            .unwrap();
        
        for value in values {
            
            if item_value == value.to_uppercase() {
                filtered_items.push(item.clone())
            }
        }
    }

    let result = ItemCollection::from_iter(filtered_items);
    Ok(result)
}

pub fn filter_items<T>(
    items: ItemCollection,
    field_name: &str,
    cmp: Cmp,
    value: T,
) -> Result<ItemCollection>
where
    T: std::convert::From<f64> + std::cmp::PartialOrd,
{
    let mut filtered_items = Vec::new();
    for item in items.items {
        let item_value: T = item.properties.additional_fields[field_name]
            .as_f64()
            .unwrap()
            .into();
        match cmp {
            Cmp::Less => {
                if item_value < value {
                    filtered_items.push(item)
                }
            }
            Cmp::Greater => {
                if item_value > value {
                    filtered_items.push(item)
                }
            }
            Cmp::LessEqual => {
                if item_value <= value {
                    filtered_items.push(item)
                }
            }
            Cmp::GreaterEqual => {
                if item_value >= value {
                    filtered_items.push(item)
                }
            }
            Cmp::Equal => {
                if item_value == value {
                    filtered_items.push(item)
                }
            }
        }
    }
    let result = ItemCollection::from_iter(filtered_items);

    Ok(result)
}
