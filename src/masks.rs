//! Module to help with masking related tasks.

use crate::{query::ImageQueryBuilder, qvf::QvfFilename, DEA};
use pyo3::prelude::*;
use std::{ops::Deref, path::PathBuf};

#[cfg(test)]
mod tests {
    use super::get_s2_cloudless_dea;
    use env_logger;
    use std::path::PathBuf;

    #[test]
    fn test_get_s2_cloudless() {
        env_logger::init();
        get_s2_cloudless_dea(
            "cfmsre_t53kru_20241110_abam5.meta",
            "test.tif",
        )
        .unwrap();
        let expect = PathBuf::from("test.tif");
        assert!(expect.exists());
        std::fs::remove_file(expect).unwrap();
        get_s2_cloudless_dea(
            "cemsre_t56jnt_20230708_abam5.meta",
            "cemsre_t56jnt_20230708_abam5.img",
        )
        .unwrap();
        let expect = PathBuf::from("cemsre_t56jnt_20230708_abam5.img");
        assert!(expect.exists());
        std::fs::remove_file(expect).unwrap();

       
    }
}
use anyhow::{bail, Result};

#[pyfunction]
pub fn get_s2_cloudless_dea(in_qvf_fn: &str, out_qvf_name: &str) -> Result<()> {
    // parse input
    let in_qvf_fn: QvfFilename = in_qvf_fn.parse().unwrap();
    // query dea base on the qvf_filename
    let source = DEA.deref().to_owned();
    let layer = &["oa_s2cloudless_mask"];
    let query = ImageQueryBuilder::from_qvf(in_qvf_fn, source, layer).expect("Invalid query");
    
    // get the result of the query
    let dst = PathBuf::from(".");
    let q = query
        .get(&dst, None, None)
        .expect("Unable to download file");
    // We assume we will get only one item. Will be a problem, I know!
    if !q.items.is_empty() {
        let asset = &q.items[0].assets["oa_s2cloudless_mask"].href;

        // tidy up
        std::fs::rename(asset, dst.join(out_qvf_name)).expect("Could not create out file");
        std::fs::remove_dir_all(PathBuf::from(&asset).parent().unwrap())
            .expect("Unable to remove folder");
    } else {
        bail!("No image found.");
    }
    Ok(())
}

#[pymodule]
fn rss_core(_py: Python, m: &PyModule) -> PyResult<()> {
    m.add_function(wrap_pyfunction!(get_s2_cloudless_dea, m)?)?;

    Ok(())
}
