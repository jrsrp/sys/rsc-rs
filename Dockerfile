ARG PROD_CONTAINER_REGISTRY
FROM ${PROD_CONTAINER_REGISTRY}/builder_rust:latest as builder

ADD . rss/
RUN cargo install --path rss
RUN ldd /opt/cargo/bin/rss | tr -s '[:blank:]' '\n' | grep '^/' | xargs -I % sh -c 'mkdir -p /opt/deps; cp % /opt/deps/;'
RUN ldd /usr/local/bin/gdal_translate | tr -s '[:blank:]' '\n' | grep '^/' | xargs -I % sh -c 'mkdir -p /opt/deps; cp % /opt/deps/;'

FROM ubuntu:22.04
COPY --from=builder  /opt/deps /usr/lib
COPY --from=builder  /rss /rss
COPY --from=builder  /opt/cargo/bin/rss /opt/bin/rss
COPY --from=builder  /usr/lib/ssl /usr/lib/ssl
COPY --from=builder  /etc/ssl /etc/ssl
COPY --from=builder /usr/local/bin/gdal_translate /usr/local/bin/gdal_translate
COPY --from=builder /usr/local/share/proj /usr/local/share/proj

RUN apt update; apt install -y ssh

COPY invoke_host_setup.sh /opt/
RUN chmod a+x /opt/invoke_host_setup.sh;  /opt/invoke_host_setup.sh


ENTRYPOINT ["/opt/bin/rss"]
CMD ["--help"]
    