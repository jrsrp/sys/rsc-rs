import click
from rss import get_s2_cloudless_dea

@click.command()
@click.option('--input', help='Input qvf file name. Used to work out date and tile')
@click.option('--output', help='Output qvf file name.')

def main(input, output):
    """ This script will download sentinel 2 cloud mask (s2cloudless) from DEA stac catalog"""
    print("Starting get_s2_cloudless")
    get_s2_cloudless_dea(input, output)

if __name__ == "__main__":
    main()
    
