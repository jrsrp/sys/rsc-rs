# This Nix flake builds Rust and Python projects (rss), supports container builds,
# and provides an easy way to run tests, lint code and build
# Python bindings using maturin.
# Available commands:
#  - `nix build`: Builds the rss binary.
#  - `nix build .#test`: Builds and runs the test suite.
#  - `nix build .#clippy`: Runs clippy to lint the code.
#  - `nix build .#container`: Builds a Docker container image including rss and runtime deps.
#  - `nix build .#pythonBinding`: Builds Python bindings using maturin.
#  - `nix develop`: Opens a development shell with all dependencies and code editor.

# Outputs:
#  - By default, the build output is placed in the Nix store and linked to the ./result folder. After building, you can run:
#  - Package build: `nix build` will produce ./result/lib/rss
#  - Python bindings (via `nix build .#pythonBinding`) will produce a `.whl` in ./result/dist.
#  - Docker container output (via `nix build .#container`) will build the container image, which can be loaded with:
#      docker load < result

{
  inputs = {
    flake-utils.url = "github:numtide/flake-utils"; # Utility functions for flakes
    naersk.url = "github:nix-community/naersk"; # Builds Rust projects using Nix
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable"; # Latest Nix packages
  };

  outputs = { self, flake-utils, naersk, nixpkgs }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        # Import the package set for the specified system (e.g., x86_64-linux)
        pkgs = (import nixpkgs) {
          inherit system;
        };

        # Import naersk for building Rust projects
        naersk' = pkgs.callPackage naersk {};

        # Define build-time dependencies
        commonNativeBuildInputs = with pkgs; [
          rustc
          cargo
          pkg-config
          clang
          opencv
          python3 # Python is needed for the Python bindings
        ];

        # Define both build-time and runtime dependencies
        commonBuildInputs = with pkgs; [
          cacert
          gdal
          openssl
          llvmPackages.libclang.lib # Required for linking with LLVM/Clang
        ];

        # Function to build the package with a specific mode (e.g., "build", "test", "clippy")
        buildWithMode = mode: naersk'.buildPackage {
          src = ./.;
          nativeBuildInputs = commonNativeBuildInputs;
          buildInputs = commonBuildInputs;
          preBuild = ''
            export export https_proxy=http://web-espproxy-usr.dmz:80;
            export LIBCLANG_PATH="${pkgs.llvmPackages.libclang.lib}/lib";
          '';
          inherit mode; # Pass the mode (build, test, clippy) to the build system
        };

        # Example: Build the project using the "build" mode
        containerBuild = buildWithMode "build";

      in rec {
        packages = {

          # Default build command, run with `nix build` or `nix run`
          default = buildWithMode "build";

          # Run tests using the "test" mode: `nix build .#test`
          test = buildWithMode "test";

          # Lint the code using clippy: `nix build .#clippy`
          clippy = buildWithMode "clippy";

          # Build a Docker container image: `nix build .#container`
          container = pkgs.dockerTools.buildImage {
            name = "my-container"; # Change this to your desired container name
            tag = "latest"; # You can change the tag if needed
            copyToRoot = pkgs.buildEnv {
              name = "my-env";
              paths = [
                containerBuild # Include the built project in the container
                pkgs.bash
                pkgs.gdal
                pkgs.openssl
                pkgs.cacert
              ];
            };
            config = {
              Cmd = [ "rss" ]; 
            };
          };

          # Python bindings build command: `nix build .#pythonBinding`
          pythonBinding = pkgs.python3Packages.buildPythonPackage {
            pname = "rss"; # Replace with your package name
            version = "1.0.0"; # Replace with your package version
            src = ./.;

            # Set up build inputs (e.g., maturin for building Python bindings)
            nativeBuildInputs = commonNativeBuildInputs ++ [pkgs.maturin];
            buildInputs = commonBuildInputs ++ [pkgs.gdal];

            # Use maturin to build the Python wheel
            buildCommand = ''
               export https_proxy=http://web-espproxy-usr.dmz:80;
               export LIBCLANG_PATH="${pkgs.llvmPackages.libclang.lib}/lib";
               export CARGO_HOME="/tmp/cargo";
               export CARGO_TARGET_DIR="/tmp/cargo_target";
               echo https_proxy is: $https_proxy ...
               mkdir -p $out/dist;
               cd $src;
               maturin pep517 build-wheel --compatibility off;

               # Copy the generated wheel file to the output directory
               cp $CARGO_TARGET_DIR/wheels/*.whl $out/dist;
               # Copy to the separate dist
               cp $out/dist/*.whl $dist;
            '';
          };
        };

        # Development shell environment: `nix develop`
        devShell = pkgs.mkShell {
          nativeBuildInputs = commonNativeBuildInputs ++ [pkgs.maturin];
          buildInputs = commonBuildInputs ++ [
            pkgs.helix # Helix editor (or replace with your preferred editor)
            pkgs.virtualenv # For creating a Python virtual environment
          ];

          # Set up the development shell with the necessary environment variables
          shellHook = ''
            export export https_proxy=http://web-espproxy-usr.dmz:80;
            export LIBCLANG_PATH="${pkgs.llvmPackages.libclang.lib}/lib";

            # Create and activate a Python virtual environment
            export VENV_DIR="$HOME/.venv/";
            rm -rf "$VENV_DIR"
            virtualenv "$VENV_DIR";
            source "$VENV_DIR/bin/activate";

            # Install the package in editable mode
            pip install -e .;
          '';
        };
      }
    );
}
