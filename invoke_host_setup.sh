#!/bin/bash
# setup dummy scripts
BIN_DIR=/usr/local/bin

function invoke_host {
    host_exe=$1
    echo \
    '#!/bin/bash
    ssh $VERBOSE_FLAG ${USER}@${HOSTNAME} ' " $host_exe " '$@' >> ${BIN_DIR}/${host_exe}

    chmod +x ${BIN_DIR}/${host_exe}
}
# for pbs
for hostcmd in qalter  qdel  qdisable  qenable  qhold  qmgr  qmove  qmsg  qorder  qrerun  qrls  qrun  qselect  qsig  qstart  qstat  qstop  qsub  qterm
do
    invoke_host $hostcmd
done

# for slurm
for hostcmd in  sacct salloc sbatch scancel sdiag sinfo sprio sreport sshare strigger sacctmgr sattach sbcast scontrol sgather smap squeue srun sstat sview
do
    invoke_host $hostcmd
done

# for dmf
for hostcmd in dmarchive  dmattr  dmcopy  dmdu  dmesg  dmfind  dmget  dmls  dmput  dmtag  dmversion
do
    invoke_host $hostcmd
done

# for sudo
invoke_host sudo
