use anyhow::Result;
use chrono::NaiveDate;
use env_logger::Env;
use rss::query::ImageQueryBuilder;
use rss::qvf::Collection;
use rss::utils::Bbox;
use rss::utils::Cmp;
use rss::utils::Intersects;
use rss::ELEMENT84;
use std::ops::Deref;
use std::path::PathBuf;

#[tokio::main]
async fn main() -> Result<()> {
    let env = Env::default().filter_or("LOG_LEVEL", "info");
    env_logger::init_from_env(env);

    let source = ELEMENT84.deref().clone();

    let bbox = Bbox {
        xmax: 141.013,
        xmin: 141.010,
        ymax: -26.0026,
        ymin: -26.0041,
    };
    let query = ImageQueryBuilder::new(
        source,
        Collection::Sentinel2,
        Intersects::Bbox(bbox),
        &["red"],
    )
    .start_date(NaiveDate::parse_from_str("20210101", "%Y%m%d")?)
    .end_date(NaiveDate::parse_from_str("20210211", "%Y%m%d")?)
    .cloudcover((Cmp::Less, 5))
    .build();
    let tmp_dir = PathBuf::from(".");
    let fc = query.get_async(&tmp_dir, 4, Some(bbox), Some("4326"));
    fc.await?;
    Ok(())
}
